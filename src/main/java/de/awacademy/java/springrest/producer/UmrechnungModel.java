package de.awacademy.java.springrest.producer;

public class UmrechnungModel {

    private String betrag;
    private String quellWaehrungCode;
    private String zielWaehrungCode;

    public UmrechnungModel() {
    }

    public UmrechnungModel(String betrag, String quellWaehrungCode, String zielWaehrungCode) {
        this.betrag = betrag;
        this.quellWaehrungCode = quellWaehrungCode;
        this.zielWaehrungCode = zielWaehrungCode;
    }

    @Override
    public String toString() {
        return "UmrechnungModel{" +
                "betrag='" + betrag + '\'' +
                ", quellWaehrungCode='" + quellWaehrungCode + '\'' +
                ", zielWaehrungCode='" + zielWaehrungCode + '\'' +
                '}';
    }

    public String getBetrag() {
        return betrag;
    }

    public void setBetrag(String betrag) {
        this.betrag = betrag;
    }

    public String getQuellWaehrungCode() {
        return quellWaehrungCode;
    }

    public void setQuellWaehrungCode(String quellWaehrungCode) {
        this.quellWaehrungCode = quellWaehrungCode;
    }

    public String getZielWaehrungCode() {
        return zielWaehrungCode;
    }

    public void setZielWaehrungCode(String zielWaehrungCode) {
        this.zielWaehrungCode = zielWaehrungCode;
    }
}
