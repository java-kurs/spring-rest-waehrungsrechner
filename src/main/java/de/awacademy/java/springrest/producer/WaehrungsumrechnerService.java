package de.awacademy.java.springrest.producer;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;

import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.MonetaryConversions;
import java.math.BigDecimal;

@Service
public class WaehrungsumrechnerService {

    /** Die EZB stellt uns aktuelle Umrechnungskurse über Moneta bereit. */
    private ExchangeRateProvider ezbUmrechnungskursProvider
            = MonetaryConversions.getExchangeRateProvider("ECB");

    /**
     *
     * @param fromAmount MonetaryAmount mit Währungscode und Betrag
     * @param toCurrencyCode Beispiel: "EUR"
     * @return Umrechnungsbetrag
     */
    public BigDecimal konvertiere(String fromCurrencyCode, BigDecimal fromAmount, String toCurrencyCode) {

        MonetaryAmount monetaryAmount = Money.of(fromAmount, fromCurrencyCode);

        CurrencyConversion ezbUmrechnung = ezbUmrechnungskursProvider.getCurrencyConversion(toCurrencyCode);
        MonetaryAmount toAmount = monetaryAmount.with(ezbUmrechnung);

        return toAmount.getNumber().numberValue(BigDecimal.class);
    }

}
