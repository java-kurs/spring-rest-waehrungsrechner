package de.awacademy.java.springrest.producer;

import de.awacademy.java.springrest.consumer.WaehrungsbetragModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class WaehrungsumrechnerController {

    @Autowired
    private WaehrungsumrechnerService waehrungsumrechnerService;

    @RequestMapping(value = "/umrechnenGet", method = RequestMethod.GET)
    public WaehrungsbetragModel umrechnen(
            @RequestParam(value = "betrag") String betrag,
            @RequestParam(value = "quellWaehrungCode") String quellWaehrungCode,
            @RequestParam(value = "zielWaehrungCode") String zielWaehrungCode) {

        BigDecimal waehrungsbetrag = waehrungsumrechnerService.konvertiere(
                quellWaehrungCode, new BigDecimal(betrag), zielWaehrungCode);

        WaehrungsbetragModel waehrungsbetragModel = new WaehrungsbetragModel(
                zielWaehrungCode, waehrungsbetrag);

        return waehrungsbetragModel;
    }

    @RequestMapping(value = "/umrechnenPost", method = RequestMethod.POST)
    public ResponseEntity<WaehrungsbetragModel> umrechnenPost(
            @RequestBody UmrechnungModel umrechnungModel) {

        BigDecimal waehrungsbetrag = waehrungsumrechnerService.konvertiere(
                umrechnungModel.getQuellWaehrungCode(),
                new BigDecimal(umrechnungModel.getBetrag()),
                umrechnungModel.getZielWaehrungCode());

        WaehrungsbetragModel waehrungsbetragModel = new WaehrungsbetragModel(
                umrechnungModel.getZielWaehrungCode(), waehrungsbetrag);

        return new ResponseEntity<>(waehrungsbetragModel, HttpStatus.OK);
    }

}
