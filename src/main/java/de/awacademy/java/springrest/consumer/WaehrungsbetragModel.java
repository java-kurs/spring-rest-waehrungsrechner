package de.awacademy.java.springrest.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class WaehrungsbetragModel {

    private String waehrungscode;
    private BigDecimal betrag;


    public WaehrungsbetragModel() {
    }

    public WaehrungsbetragModel(String waehrungscode, BigDecimal betrag) {
        this.waehrungscode = waehrungscode;
        this.betrag = betrag;
    }

    @Override
    public String toString() {
        return "WaehrungsbetragModel{" +
                "waehrungscode='" + waehrungscode + '\'' +
                ", betrag=" + betrag +
                '}';
    }

    // Getter und Setter für die Attribute

    public String getWaehrungscode() {
        return waehrungscode;
    }

    public void setWaehrungscode(String waehrungscode) {
        this.waehrungscode = waehrungscode;
    }

    public BigDecimal getBetrag() {
        return betrag;
    }

    public void setBetrag(BigDecimal betrag) {
        this.betrag = betrag;
    }
}
