package de.awacademy.java.springrest.consumer;

import de.awacademy.java.springrest.producer.UmrechnungModel;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

public class WaehrungsumrechnerClient {

    private static final String URL = "http://localhost:8080/umrechnenPost";

    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();

        UmrechnungModel umrechnungModel = new UmrechnungModel("10", "EUR", "GBP");
        HttpEntity<UmrechnungModel> request = new HttpEntity<>(umrechnungModel);
        WaehrungsbetragModel waehrungsbetragModel = restTemplate.postForObject(URL, request, WaehrungsbetragModel.class);

        System.out.println(waehrungsbetragModel);
    }
}
