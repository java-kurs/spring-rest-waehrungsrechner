package de.awacademy.java.springrest;

import de.awacademy.java.springrest.consumer.WaehrungsbetragModel;
import de.awacademy.java.springrest.producer.UmrechnungModel;
import de.awacademy.java.springrest.producer.WaehrungsumrechnerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringRestApplicationTests {

    private static final String URL = "http://localhost:8080/umrechnenPost";

    @Autowired
    private WaehrungsumrechnerService waehrungsumrechnerService;

	@Test
	public void contextLoads() {
    }

}
