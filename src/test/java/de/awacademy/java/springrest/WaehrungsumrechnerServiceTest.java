package de.awacademy.java.springrest;

import de.awacademy.java.springrest.producer.WaehrungsumrechnerService;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertNotNull;

public class WaehrungsumrechnerServiceTest {

    private WaehrungsumrechnerService waehrungsumrechner = new WaehrungsumrechnerService();

    @Test
    public void konvertiere10EuroInDollar() {

        BigDecimal umrechungsbetrag = waehrungsumrechner.konvertiere(
                "EUR", BigDecimal.valueOf(10), "USD");

        System.out.println(umrechungsbetrag);

        assertNotNull("Kein Umrechnungsbetrag ermittelt");
    }
}
